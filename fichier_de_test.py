# coding: utf-8
import argparse
import random
import client
import const
import plateau
import joueur
import matrice

# True si on veut que le joueur peigne en fonction du temps et de sa reserve
timing_pour_peindre = False


def voisins(le_plateau, pos):
    """
    trouve les cases voisines de la position pos
    args:
                    pos: un tuple la position ligne,colonne
    return un ensemble des position accesible
    """
    ens = set()
    dico = plateau.directions_possibles(le_plateau, pos)
    for cle in dico:
        val = (plateau.INC_DIRECTION[cle][0]+pos[0],
               plateau.INC_DIRECTION[cle][1]+pos[1])
        ens.add(val)
    return ens


def fabrique_le_calque(le_plateau, position_depart):
    """fabrique le calque d'un labyrinthe en utilisation
    le principe de l'inondation :

    Args:
                    le_plateau (plateau): un plateau de jeu
                    position_depart (tuple): un tuple de deux entiers de la forme
                    (no_ligne, no_colonne)

    Returns:
                    matrice: une matrice qui a la taille du plateau dont la case
                    qui se trouve à la
             position_de_depart est à 0 les autres cases contiennent la longueur du
             plus court chemin pour y arriver
             (les murs et les cases innaccessibles sont à None)
    """
    ma_matrice = matrice.new_matrice(plateau.get_nb_lignes(
        le_plateau), plateau.get_nb_colonnes(le_plateau), None)
    matrice.set_val(ma_matrice, position_depart[0], position_depart[1], 0)
    ensemble_voisins = voisins(
        le_plateau, (position_depart[0], position_depart[1]))
    valeur = 0
    while ensemble_voisins != set():
        ens = set()
        valeur += 1
        for voisin in ensemble_voisins:

            matrice.set_val(ma_matrice, voisin[0], voisin[1], valeur)
            ses_voisin = voisins(le_plateau, (voisin[0], voisin[1]))
            for voisin2 in ses_voisin:
                if matrice.get_val(ma_matrice, voisin2[0], voisin2[1]) is None:
                    ens.add(voisin2)
        ensemble_voisins = ens
    return ma_matrice


def ennemi_a_proximite(plan, pos, joueur):
    """Cette fonction permet de savoir si un ennemi est à proximité du joueur
    Args:
                    plan (str): le plateau de jeu
                    pos (str): la position du joueur
    Returns:
                    bool: True si un ennemi est à proximité, False sinon
    """
    # Vérifier si un ennemi est à proximité
    distance = const.DIST_MAX
    if joueur['reserve'] < const.DIST_MAX:
        distance = joueur['reserve']
    directions = plateau.directions_possibles(plan, pos)
    for direction in directions:
        if joueur['objet'] == const.PISTOLET:

            for i in range(1, distance + 1):
                position = pos[0] + i * plateau.INC_DIRECTION[direction][0],
                pos[1] + \
                    i * plateau.INC_DIRECTION[direction][1]
                if position in plan["plateau"].keys():
                    if plateau.get_case(plan, position)["joueurs"] != set():

                        return (True, direction)
                else:
                    break
        else:
            for i in range(1,   distance + 1):
                position = pos[0] + i * plateau.INC_DIRECTION[direction][0],
                pos[1] + \
                    i * plateau.INC_DIRECTION[direction][1]
                if position in plan["plateau"].keys():
                    if plan['plateau'][position]['mur']:
                        break
                    if plateau.get_case(plan, position)["joueurs"] != set():

                        return (True, direction)
                else:
                    break

    return (False, None)


def vers_sa_zone(le_plateau, couleur, pos):
    """
    fonction qui calcule le chemin le plus proche entre sa zone est
    sa position (fonction utile , si il n'a plus de reserve de peinture)
    Args:
                    le_plateau: un plateau de jeu
                    couleur: la couleur du joueur
                    pos: sa position sur le plateau
    return:
                    liste: une liste de tuple pour aller vers sa case
    """
    nb = plateau.get_nb_lignes(le_plateau)*plateau.get_nb_colonnes(le_plateau)
    liste = nb*[0]
    for ligne in range(plateau.get_nb_lignes(le_plateau)):
        for colonne in range(plateau.get_nb_colonnes(le_plateau)):
            if le_plateau['plateau'][ligne, colonne]['couleur'] == couleur and not le_plateau['plateau'][ligne, colonne]['mur']:
                liste_chemin_moment = fabrique_chemin_prendre_objet(
                    le_plateau, pos, (ligne, colonne))
                verif = len(liste_chemin_moment) > 0 and len(liste_chemin_moment) < len(liste)
                if verif :
                    liste = liste_chemin_moment
    return liste


def chasser_ennemi(le_plateau, couleur, pos):
    """
    fonction qui calcule le chemin le plus proche entre sa position et  un ennemi
    Args:
                    le_plateau: un plateau de jeu
                    couleur: la couleur du joueur
                    pos: sa position sur le plateau
    return:
                    liste: une liste de tuple pour aller vers sa case
    """
    nb = plateau.get_nb_lignes(le_plateau)*plateau.get_nb_colonnes(le_plateau)
    liste = nb*[0]
    for ligne in range(plateau.get_nb_lignes(le_plateau)):
        for colonne in range(plateau.get_nb_colonnes(le_plateau)):
            if le_plateau['plateau'][ligne, colonne]["joueurs"] != set():
                le_plateau['plateau'][ligne,
                                      colonne]["joueurs"].discard(couleur)
                if le_plateau['plateau'][ligne, colonne]["joueurs"] != set():
                    liste_chemin_moment = fabrique_chemin_prendre_objet(
                        le_plateau, pos, (ligne, colonne))
                    verif = len(liste_chemin_moment) > 0 and len(liste_chemin_moment) < len(liste)
                    if verif :   
                        liste = liste_chemin_moment
    return liste


def fabrique_chemin_prendre_objet(le_plateau, pos_joueur, pos_objet):
    """
    fonction qui fabrique le chemin le plus rapide pour aller vers un objet ou une position
    ARGS:
                    le_plateau: le plateau du jeu
                    pos: position du joueur
                    objet: position du bouclier
    return :
    liste: une liste de tuple pour aller vers l'objet
    """
    liste = [pos_objet]
    le_calque = fabrique_le_calque(le_plateau, pos_joueur)
    if matrice.get_val(le_calque, pos_objet[0], pos_objet[1]) is None:
        return []
    while pos_objet != pos_joueur:
        pos_mini = pos_objet
        voisin = voisins(le_plateau, pos_objet)
        for element in voisin:

            if matrice.get_val(le_calque, element[0], element[1])+1 == matrice.get_val(le_calque, pos_objet[0], pos_objet[1]):
                pos_mini = element
        liste.append(pos_mini)
        pos_objet = pos_mini
    liste.pop()
    return liste[::-1]


def Maximiser_peinture(plan, joueur):
    """fonction pour savoir dans quelle direction maximiser la peinture

Args:
            plan : le plateau du jeu
            joueur : le dictionnaire des informations du joueur

Returns:
             str: une direction
    """
    res = ["", 0, 0]
    reserve = joueur['reserve']
    pos = joueur['position']
    compt = 0
    le_plateau = plan
    distance = reserve
    direc = ["N", "S", "E", "O"]
    couleur = False
    ind = 0
    for valeur in direc:
        
        if joueur["objet"] == const.PISTOLET:
            while distance > 0 and (pos[0]+ind*plateau.INC_DIRECTION[valeur][0], pos[1]+ind*plateau.INC_DIRECTION[valeur][1]) in le_plateau["plateau"].keys():
                if le_plateau["plateau"][pos[0]+ind*plateau.INC_DIRECTION[valeur][0], pos[1]+ind*plateau.INC_DIRECTION[valeur][1]]["joueurs"] != set():
                    le_plateau["plateau"][pos[0]+ind*plateau.INC_DIRECTION[valeur][0], pos[1] +
                                          ind*plateau.INC_DIRECTION[valeur][1]]["joueurs"].discard(joueur["couleur"])
                    if le_plateau["plateau"][pos[0]+ind*plateau.INC_DIRECTION[valeur][0], pos[1]+ind*plateau.INC_DIRECTION[valeur][1]]["joueurs"] != set():
                        compt += 3
                if le_plateau["plateau"][pos[0]+ind*plateau.INC_DIRECTION[valeur][0], pos[1]+ind*plateau.INC_DIRECTION[valeur][1]]["couleur"] == joueur["couleur"]:
                    compt -= 1
                    ind += 1
                    distance -= 1
                elif le_plateau["plateau"][pos[0]+ind*plateau.INC_DIRECTION[valeur][0], pos[1]+ind*plateau.INC_DIRECTION[valeur][1]]["couleur"] == " ":
                    compt += 1
                    ind += 1
                    distance -= 1
                    couleur = True

                elif le_plateau["plateau"][pos[0]+ind*plateau.INC_DIRECTION[valeur][0], pos[1]+ind*plateau.INC_DIRECTION[valeur][1]]["mur"]:
                    compt += 3
                    ind += 1
                    distance -= 1
                    couleur = True

                else:
                    compt += 1
                    ind += 1
                    distance -= 2
                    couleur = True
        else:
             while distance > 0 and (pos[0]+ind*plateau.INC_DIRECTION[valeur][0], pos[1]+ind*plateau.INC_DIRECTION[valeur][1]) in le_plateau["plateau"].keys() and not le_plateau["plateau"][pos[0]+ind*plateau.INC_DIRECTION[valeur][0], pos[1]+ind*plateau.INC_DIRECTION[valeur][1]]["mur"]:
                
                if le_plateau["plateau"][pos[0]+ind*plateau.INC_DIRECTION[valeur][0], pos[1]+ind*plateau.INC_DIRECTION[valeur][1]]["joueurs"] != set():
                    le_plateau["plateau"][pos[0]+ind*plateau.INC_DIRECTION[valeur][0], pos[1] +
                                          ind*plateau.INC_DIRECTION[valeur][1]]["joueurs"].discard(joueur["couleur"])
                    if le_plateau["plateau"][pos[0]+ind*plateau.INC_DIRECTION[valeur][0], pos[1]+ind*plateau.INC_DIRECTION[valeur][1]]["joueurs"] != set():
                        compt += 3
                if le_plateau["plateau"][pos[0]+ind*plateau.INC_DIRECTION[valeur][0], pos[1]+ind*plateau.INC_DIRECTION[valeur][1]]["couleur"] == joueur["couleur"]:
                    compt -= 1
                    ind += 1
                    distance -= 1
                elif le_plateau["plateau"][pos[0]+ind*plateau.INC_DIRECTION[valeur][0], pos[1]+ind*plateau.INC_DIRECTION[valeur][1]]["couleur"] == " ":
                    compt += 1
                    ind += 1
                    distance -= 1
                    couleur = True
                else:
                    compt += 1
                    ind += 1
                    distance -= 2
                    couleur = True
        if compt >= res[1]:
            res[1] = compt
            res[0] = valeur
            res[2] = distance
        if compt == res[1] and distance > res[2]:
            res[1] = compt
            res[0] = valeur
            res[2] = distance
        ind = 0
        compt = 0
        distance = reserve
    if not couleur:
        res[0] = "X"
    if res[0] == "" :
        res[0] = "X"
    return res


def reste_dans_zone(le_plateau, pos, joueur):
    """Fonction qui va faire en sorte que le joueur reste dans sa zone
    Args:
                    le_plateau (dict): le plateau du jeu
                    pos (tuple): position du joueur
                    joueur (dict): le joueur
    Returns:
                    str: la direction dans laquelle le joueur doit se diriger
    """

    #    return go(plan,pos,vers_sa_zone(plan,joueur['couleur'],pos))
    directions = plateau.directions_possibles(le_plateau, pos)
    for direction in directions:
        if le_plateau['plateau'][pos[0]+plateau.INC_DIRECTION[direction][0], pos[1]+plateau.INC_DIRECTION[direction][1]]["couleur"] == joueur["couleur"]:
            return direction
    for direction in directions:
        if le_plateau['plateau'][pos[0]+plateau.INC_DIRECTION[direction][0], pos[1]+plateau.INC_DIRECTION[direction][1]]["couleur"] == " ":
            return direction
    return random.choice(directions.keys())


def case_vide(le_plateau, pos):
    directions = plateau.directions_possibles(le_plateau, pos)
    for direction in directions:
        if le_plateau['plateau'][pos[0]+plateau.INC_DIRECTION[direction][0], pos[1]+plateau.INC_DIRECTION[direction][1]]["couleur"] == " ":
            return direction
    return random.choice(directions.keys())


def nos_case(le_plateau, pos, joueur):
    """fonction qui permettre de se diriger vers une case qui nous appartient

Args:
            le_plateau : le plateau du jeu
            pos : position du joueur
            joueur : le joueur

Returns:
            str: directuion dans laquelle le joueur doit se diriger
    """
    directions = plateau.directions_possibles(le_plateau, pos)
    for direction in directions:
        if le_plateau['plateau'][pos[0]+plateau.INC_DIRECTION[direction][0], pos[1]+plateau.INC_DIRECTION[direction][1]]["couleur"] == joueur['couleur']:
            return direction
    for direction in directions:
        if le_plateau['plateau'][pos[0]+plateau.INC_DIRECTION[direction][0], pos[1]+plateau.INC_DIRECTION[direction][1]]["couleur"] == " ":
            return direction
    for direction in directions:
        if le_plateau['plateau'][pos[0]+plateau.INC_DIRECTION[direction][0], pos[1]+plateau.INC_DIRECTION[direction][1]]["couleur"] != " " and le_plateau['plateau'][pos[0]+plateau.INC_DIRECTION[direction][0], pos[1]+plateau.INC_DIRECTION[direction][1]]["couleur"] != joueur['couleur']:
            return direction


def go(le_plateau, joueur, chemin):
    """Fonction qui va définir le déplacement du joueur
    Args:
                    le_plateau (dict): le plateau du jeu
                    joueur (dict): le joueur
                    chemin (list): le chemin que doit suivre le joueur
    Returns:
                    str: la direction dans laquelle le joueur doit se diriger
    """
    pos_voulue = chemin[0]
    pos = joueur["position"]
    if pos_voulue[0] == pos[0]:
        if pos_voulue[1] > pos[1]:
            return "E"
        else:
            return "O"
    else:
        if pos_voulue[0] > pos[0]:
            return "S"
        else:
            return "N"


def est_dans_couleur(le_plateau, pos, joueur):
    """Fonction qui va définir si le joueur est dans sa zone
    Args:
                    le_plateau (dict): le plateau du jeu
                    pos (tuple): position du joueur
                    joueur (dict): le joueur
    Returns:
                    bool: True si le joueur est dans sa zone, False sinon
    """
    if le_plateau['plateau'][pos]["couleur"] == joueur["couleur"]:
        return True
    else:
        return False


def part_de_sa_zone(le_plateau, couleur, pos):
    """
    fonction qui calcule le chemin le plus proche entre sa zone est sa position (fonction utile , si il n'a plus de reserve de peinture)
    Args:
                    le_plateau: un plateau de jeu
                    couleur: la couleur du joueur
                    pos: sa position sur le plateau
    return:
                    liste: une liste de tuple pour aller vers sa case
    """
    nb = plateau.get_nb_lignes(le_plateau)*plateau.get_nb_colonnes(le_plateau)
    liste = nb*[0]
    for ligne in range(plateau.get_nb_lignes(le_plateau)):
        for colonne in range(plateau.get_nb_colonnes(le_plateau)):
            if le_plateau['plateau'][ligne, colonne]['couleur'] != couleur and not le_plateau['plateau'][ligne, colonne]['mur']:
                liste_chemin_moment = fabrique_chemin_prendre_objet(
                    le_plateau, pos, (ligne, colonne))
                verif = len(liste_chemin_moment) > 0 and len(liste_chemin_moment) < len(liste)
                if verif :
                    liste = liste_chemin_moment
    return liste

def chemin_objet(le_plateau, pos_joueur):
    """Fonction qui va définir le chemin le plus court pour aller vers un objet
    Args:
                    le_plateau (dict): le plateau du jeu
                    pos_joueur (tuple): position du joueur
    Returns:
                    list: le chemin le plus court pour aller vers un objet
    """
    liste = []
    for ligne in range(plateau.get_nb_lignes(le_plateau)):
        for colonne in range(plateau.get_nb_colonnes(le_plateau)):
            if le_plateau['plateau'][ligne, colonne]['objet'] != const.AUCUN:
                liste_chemin_moment = fabrique_chemin_prendre_objet(
                    le_plateau, pos_joueur, (ligne, colonne))
                if len(liste_chemin_moment) <= 2:
                    liste = liste_chemin_moment
    return liste


def chemin_objet2(le_plateau, pos_joueur):
    """Fonction qui va définir le chemin le plus court pour aller vers un objet
            Args:
                            le_plateau (dict): le plateau du jeu
                            pos_joueur (tuple): position du joueur
            Returns:
                            list: le chemin le plus court pour aller vers un objet
            """
    nb = plateau.get_nb_lignes(le_plateau)*plateau.get_nb_colonnes(le_plateau)
    liste = [0]*nb
    for ligne in range(plateau.get_nb_lignes(le_plateau)):
        for colonne in range(plateau.get_nb_colonnes(le_plateau)):
            if le_plateau['plateau'][ligne, colonne]['objet'] != const.AUCUN:
                liste_chemin_moment = fabrique_chemin_prendre_objet(
                    le_plateau, pos_joueur, (ligne, colonne))
                if len(liste) <= len(liste_chemin_moment):
                    liste = liste_chemin_moment
    if liste == [0]*nb:
        return []
    return liste


def chemin_bidon(le_plateau, pos_joueur):
    """Fonction qui va définir le chemin le plus court pour aller vers un bidon
    Args:
    le_plateau (dict): le plateau du jeu
    pos_joueur (tuple): position du joueur
    Returns:
    list: le chemin le plus court pour aller vers un bidon
    """
    nb = plateau.get_nb_lignes(le_plateau)*plateau.get_nb_colonnes(le_plateau)
    liste = [0]*nb
    for ligne in range(plateau.get_nb_lignes(le_plateau)):
        for colonne in range(plateau.get_nb_colonnes(le_plateau)):
            if le_plateau['plateau'][ligne, colonne]['objet'] == const.BIDON:
                liste_chemin_moment = fabrique_chemin_prendre_objet(
                    le_plateau, pos_joueur, (ligne, colonne))
                if len(liste_chemin_moment) <= len(liste):
                    liste = liste_chemin_moment
    if liste == [0]*nb:
        return []
    return liste

def chemin_pistolet(le_plateau, pos_joueur):
    """Fonction qui va définir le chemin le plus court pour aller vers un bidon
    Args:
    le_plateau (dict): le plateau du jeu
    pos_joueur (tuple): position du joueur
    Returns:
    list: le chemin le plus court pour aller vers un bidon
    """
    nb = plateau.get_nb_lignes(le_plateau)*plateau.get_nb_colonnes(le_plateau)
    liste = [0]*nb
    for ligne in range(plateau.get_nb_lignes(le_plateau)):
        for colonne in range(plateau.get_nb_colonnes(le_plateau)):
            if le_plateau['plateau'][ligne, colonne]['objet'] == const.PISTOLET:
                liste_chemin_moment = fabrique_chemin_prendre_objet(
                    le_plateau, pos_joueur, (ligne, colonne))
                if len(liste_chemin_moment) <= len(liste):
                    liste = liste_chemin_moment
    if liste == [0]*nb:
        return []
    return liste


def mon_IA(ma_couleur, carac_jeu, plan, les_joueurs):
    """ Cette fonction permet de calculer les deux actions du joueur de couleur ma_couleur
                    en fonction de l'état du jeu décrit par les paramètres. 
                    Le premier caractère est parmi XSNOE X indique pas de peinture et les autres
                    caractères indique la direction où peindre (Nord, Sud, Est ou Ouest)
                    Le deuxième caractère est parmi SNOE indiquant la direction où se déplacer.

    Args:
                    ma_couleur (str): un caractère en majuscule indiquant la couleur du joueur
                    carac_jeu (str): une chaine de caractères contenant les caractéristiques
                                                                                                                             de la partie séparées par des ;
                                     duree_act;duree_tot;reserve_init;duree_obj;penalite;bonus_touche;bonus_rechar;bonus_objet           
                    plan (str): le plan du plateau comme indiqué dans le sujet
                    les_joueurs (str) : la liste des joueurs avec leur caractéristique (1 joueur par ligne)
                    couleur;reserve;nb_cases_peintes;objet;duree_objet;ligne;colonne;nom_complet

    Returns :
                    str : une chaine de deux caractères en majuscules indiquant la direction de peinture
                                    et la direction de déplacement
    """
    joueurs = les_joueurs.split('\n')
    liste_ennemie = []
    for valeur in joueurs:
        joueurs = joueur.joueur_from_str(valeur)
        if joueurs['couleur'] != ma_couleur:
            liste_ennemie.append(joueurs)
        else:
            moi = joueurs
    plan = plateau.Plateau(plan)
    carac_jeu = carac_jeu.split(';')
    carac_jeu = {'duree_act': int(carac_jeu[0]), 'duree_tot': int(carac_jeu[1]), 'reserve_init': int(carac_jeu[2]), 'duree_objet': int(
        carac_jeu[3]), 'penalite': int(carac_jeu[4]), 'bonus_touche': int(carac_jeu[5]), 'bonus_rechar': int(carac_jeu[6]), 'bonus_objet': int(carac_jeu[7])}
    #Premier tour
    if carac_jeu['duree_act'] == 0: 
        return Maximiser_peinture(plan,  moi)[0]*2
    nb_cases = plateau.get_nb_lignes(plan)*plateau.get_nb_colonnes(plan)
    if moi['reserve'] >= nb_cases:
        globals()["timing_pour_peindre"] = True
    #20 tours pour stocker le plus de peinture possible

    if carac_jeu['duree_act'] <= 20 and not globals()["timing_pour_peindre"]:
        if 1 <= len(chemin_objet(plan, moi['position'])) <= 3:          #si l'objet est proche de 3 case
            if ennemi_a_proximite(plan, moi['position'], moi)[0]:       #si ennemi proche
                return ennemi_a_proximite(plan, moi['position'], moi)[1] + go(plan, moi, chemin_objet(plan, moi['position']))   
                #regard si c'est bien de tirer et avance vers l'objet
            return Maximiser_peinture(plan,  moi)[0] + go(plan, moi, chemin_objet(plan, moi['position']))
            #sinon tu maximise la peinture tout en allant vers l'objet
        if ennemi_a_proximite(plan, moi['position'], moi)[0]:
            return ennemi_a_proximite(plan, moi['position'], moi)[1] + reste_dans_zone(plan, moi['position'], moi)
            #si il y a un ennemi tu tirs et tu reste dans ta zone
        if not est_dans_couleur(plan, moi['position'], moi):
            return Maximiser_peinture(plan,  moi)[0]*2
            #si tu n'est pas dans ta couleur tu tirs au meilleur endroit et avance vers cette endroit
        if moi['surface'] <= 1: # si ta plus de surface
            return Maximiser_peinture(plan,  moi)[0]*2      #tu tirs et tu de deplace dans cette zone
        return "X" + reste_dans_zone(plan, moi['position'], moi)        #sinon tu tirs pas et tu reste dans ta zone
    # Apres les 20 tours on peint le plus possible
    else:
      # Si on n'a plus de reserve
        if moi['reserve'] <= 0 and moi['surface'] < 0:
            if chemin_bidon(plan, moi['position']) != []:
                return "X" + go(plan, moi, chemin_bidon(plan, moi['position']))
            if go(plan, moi, chemin_objet2(plan, moi['position'])) != []:
                return "X" + go(plan, moi, chemin_objet2(plan, moi['position']))
            else:
                directions = plateau.directions_possibles(
                    plan, moi['position'])
                return "X" + random.choice(directions.keys())
        elif moi['reserve'] <= 0 and moi['surface'] >= 1 and est_dans_couleur(plan, moi['position'], moi):
            return "X" + reste_dans_zone(plan, moi['position'], moi)
        elif moi['reserve'] <= 0 and moi['surface'] > 1 and not est_dans_couleur(plan, moi['position'], moi):
            if len(vers_sa_zone(plan, moi['couleur'], moi['position'])) == 0:
                return "X" + nos_case(plan, moi['position'], moi)
            return "X" + go(plan, moi, vers_sa_zone(plan,moi['couleur'], moi['position']))
      # Si il y a un objet a proximite
        elif 1 <= len(chemin_objet(plan, moi['position'])) <= 2:
            if ennemi_a_proximite(plan, moi['position'], moi)[0]:
                return ennemi_a_proximite(plan, moi['position'], moi)[1] + go(plan, moi, chemin_objet(plan, moi['position']))
            return Maximiser_peinture(plan,  moi)[0] + go(plan, moi, chemin_objet(plan, moi['position']))
      # Maximise la peinture
        else:
            print("reserve")
            # Si un tir n'apporte rien
            if Maximiser_peinture(plan,  moi)[0] == 'X': 
              # Si on ne peut pas sortir de sa zone
                if len(part_de_sa_zone(plan, moi['couleur'], moi['position'])) == 0:
                  # Si il ya un pistolet a aller prendre
                    if len(chemin_pistolet(plan, moi['position'])) == 0:
                        return "X" + go(plan, moi, chasser_ennemi(plan,moi['couleur'],  moi['position']))
                    return 'X' + go(plan, moi, chemin_pistolet(plan, moi['position']))
                return 'X' + go(plan, moi, part_de_sa_zone(plan, moi['couleur'], moi['position']))
            # Maximise la peinture
            else:
                return Maximiser_peinture(plan,  moi)[0]*2


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--equipe", dest="nom_equipe",
                        help="nom de l'équipe", type=str, default='Non fournie')
    parser.add_argument("--serveur", dest="serveur",
                        help="serveur de jeu", type=str, default='localhost')
    parser.add_argument("--port", dest="port",
                        help="port de connexion", type=int, default=1111)

    args = parser.parse_args()
    le_client = client.ClientCyber()
    le_client.creer_socket(args.serveur, args.port)
    le_client.enregistrement(args.nom_equipe, "joueur")
    ok = True
    while ok:
        ok, id_joueur, le_jeu = le_client.prochaine_commande()
        if ok:
            carac_jeu, le_plateau, les_joueurs = le_jeu.split(
                "--------------------\n")
            actions_joueur = mon_IA(
                id_joueur, carac_jeu, le_plateau, les_joueurs[:-1])
            le_client.envoyer_commande_client(actions_joueur)
            # le_client.afficher_msg("sa reponse  envoyée "+str(id_joueur)+args.nom_equipe)
    le_client.afficher_msg("terminé")