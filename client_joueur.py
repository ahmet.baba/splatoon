# coding: utf-8
import argparse
import random
import client
import const
import plateau
import case
import joueur

def mon_IA(ma_couleur,carac_jeu, plan, les_joueurs):
    """ Cette fonction permet de calculer les deux actions du joueur de couleur ma_couleur
        en fonction de l'état du jeu décrit par les paramètres. 
        Le premier caractère est parmi XSNOE X indique pas de peinture et les autres
        caractères indique la direction où peindre (Nord, Sud, Est ou Ouest)
        Le deuxième caractère est parmi SNOE indiquant la direction où se déplacer.

    Args:
        ma_couleur (str): un caractère en majuscule indiquant la couleur du jeur
        carac_jeu (str): une chaine de caractères contenant les caractéristiques
                                   de la partie séparées par des ;
             duree_act;duree_tot;reserve_init;duree_obj;penalite;bonus_touche;bonus_rechar;bonus_objet           
        plan (str): le plan du plateau comme comme indiqué dans le sujet
        les_joueurs (str): le liste des joueurs avec leur caractéristique (1 joueur par ligne)
        couleur;reserve;nb_cases_peintes;objet;duree_objet;ligne;colonne;nom_complet
    
    Returns:
        str: une chaine de deux caractères en majuscules indiquant la direction de peinture
            et la direction de déplacement
    """
    # IA complètement aléatoire
    # ici il faudra décoder le plan, les joueur et les caractéristiques du jeu
    return random.choice("XNSOE")+random.choice("NSEO")

if __name__=="__main__":
    parser = argparse.ArgumentParser()  
    parser.add_argument("--equipe", dest="nom_equipe", help="nom de l'équipe", type=str, default='Non fournie')
    parser.add_argument("--serveur", dest="serveur", help="serveur de jeu", type=str, default='localhost')
    parser.add_argument("--port", dest="port", help="port de connexion", type=int, default=1111)
    
    args = parser.parse_args()
    le_client=client.ClientCyber()
    le_client.creer_socket(args.serveur,args.port)
    le_client.enregistrement(args.nom_equipe,"joueur")
    ok=True
    while ok:
        ok,id_joueur,le_jeu=le_client.prochaine_commande()
        if ok:
            carac_jeu,le_plateau,les_joueurs=le_jeu.split("--------------------\n")
            actions_joueur=mon_IA(id_joueur,carac_jeu,le_plateau,les_joueurs[:-1])
            le_client.envoyer_commande_client(actions_joueur)
            # le_client.afficher_msg("sa reponse  envoyée "+str(id_joueur)+args.nom_equipe)
    le_client.afficher_msg("terminé")

    
"""
    if moi['reserve']<=0 and part_de_sa_zone(plan,moi['couleur'],moi['position']) !=[]:
        if chemin_bidon(plan,moi['position']) != []:
            return "X"+ go(plan,moi,chemin_bidon(plan,moi['position']))
        else:
            return "X" +go(plan,moi,chemin_objet2(plan,moi['position']))
    
    
    
    if chemin_objet(plan,moi['position']) !=[]:
        deplacement = go(plan,moi,chemin_objet(plan,moi['position']))
        
        action_peinture = Maximiser_peinture(plan, moi['position'],moi, moi['reserve'])[0]
        #action_deplacement = Maximiser_deplacement(plan,moi['position'], moi)[0]
        return action_peinture + deplacement
    


    if ennemi_a_proximite(plan, moi["position"], moi)[0] == True:           #si ennemi proche
        action_peinture = ennemi_a_proximite(plan, moi["position"], moi )[1]
        return action_peinture*2

    if moi["reserve"]<=0 :              #si plus de reserve
        if est_dans_couleur(plan,moi["position"], moi):
            action_deplacement = reste_dans_zone(plan,moi["position"], moi)
            return "X" + action_deplacement

    if  Maximiser_peinture(plan, moi['position'],moi, moi['reserve'])[0] =="X":
        if part_de_sa_zone(plan,moi['couleur'],moi['position']) !=[]:
            return "X"+go(plan,moi,part_de_sa_zone(plan,moi['couleur'],moi['position']))
        directions = plateau.directions_possibles(plan,moi['position'])
        direc=""
        for cle,val in directions.items():
            if val == ' ':
                return "X" +cle
            direc +=cle
        return "X" + random.choice(direc)

    elif carac_jeu['duree_act'] == 0:
        return Maximiser_peinture(plan, moi["position"],moi,moi['reserve'])[0]*2 

    

    elif carac_jeu["duree_act"]<=10:
        action_peinture = Maximiser_peinture(plan, moi['position'],moi, moi['reserve'])[0]
        action_deplacement = Maximiser_deplacement(plan,moi['position'], moi)[0]
        return action_peinture*2 # + action_deplacement

    elif carac_jeu['duree_act']>10 and carac_jeu['duree_act']<=50:
        
        if ennemi_a_proximite(plan, moi["position"], moi)[0] == True:
            action_peinture = ennemi_a_proximite(plan, moi["position"], moi)[1]
        else:
            if moi["reserve"] > 0:
                action_peinture = Maximiser_peinture(plan, moi["position"],moi, moi['resrve'])[0]
                action_deplacement = go(plan, moi['couleur'],vers_sa_zone(plan, moi["position"], moi['couleur']))
            else:
                action_peinture = "X"
                chemin= fabrique_chemin_prendre_objet(plan, moi["position"], moi["couleur"])
                action_deplacement=go(plan, moi['couleur'],chemin)
        return action_peinture *2 #+ action_deplacement

    elif carac_jeu['duree_act']>50 and carac_jeu['duree_act']<=150:
        if ennemi_a_proximite(plan, moi["position"], moi)[0] == True:
            action_peinture = ennemi_a_proximite(plan, moi["position"], moi)[1]
        else:
            action_peinture = Maximiser_peinture(plan, moi['position'],moi, moi['reserve'])[0]
            action_deplacement = Maximiser_deplacement(plan,moi['position'], moi)[0]
            return action_peinture *2 #+ action_deplacement
"""