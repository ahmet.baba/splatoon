"""module de gestion du plateau de jeu
"""
import const
import case

# dictionnaire permettant d'associer une direction et la position relative
# de la case qui se trouve dans cette direction
INC_DIRECTION = {'N': (-1, 0), 'E': (0, 1), 'S': (1, 0),
                 'O': (0, -1), 'X': (0, 0)}


def get_nb_lignes(plateau):
    """retourne le nombre de lignes du plateau

    Args:
        plateau (dict): le plateau considéré

    Returns:
        int: le nombre de lignes du plateau
    """
    return int(plateau['x']) # 'x' est l'entier réprésentant le nombre de lignes dans notre plateau


def get_nb_colonnes(plateau):
    """retourne le nombre de colonnes du plateau

    Args:
        plateau (dict): le plateau considéré

    Returns:
        int: le nombre de colonnes du plateau
    """
    return int(plateau['y']) # 'y' est l'entier réprésentant le nombre de colonnes dans notre plateau


def get_case(plateau, pos):
    """retourne la case qui se trouve à la position pos du plateau

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire (lig,col) de deux int

    Returns:
        dict: La case qui se situe à la position pos du plateau
    """
    return plateau['plateau'][pos] # on retourne la case qui se trouve à la position pos du plateau


def poser_joueur(plateau, joueur, pos):
    """pose un joueur en position pos sur le plateau

    Args:
        plateau (dict): le plateau considéré
        joueur (str): la lettre représentant le joueur
        pos (tuple): une paire (lig,col) de deux int
    """
    plateau['plateau'][pos]['joueurs'] = set() # on initialise le set des joueurs
    plateau['plateau'][pos]['joueurs'].add(joueur) # on ajoute le joueur à la case


def poser_objet(plateau, objet, pos):
    """Pose un objet en position pos sur le plateau. Si cette case contenait déjà
        un objet ce dernier disparait

    Args:
        plateau (dict): le plateau considéré
        objet (int): un entier représentant l'objet. const.AUCUN indique aucun objet
        pos (tuple): une paire (lig,col) de deux int
    """
    plateau['plateau'][pos]['objet'] = objet # on met le paramètre objet dans la clé objet de la case


def plateau_from_str(la_chaine):
    """Construit un plateau à partir d'une chaine de caractère contenant les informations
        sur le contenu du plateau (voir sujet)

    Args:
        la_chaine (str): la chaine de caractères décrivant le plateau

    Returns:
        dict: le plateau correspondant à la chaine. None si l'opération a échoué
"""
    ligne = la_chaine.split("\n") # on sépare la chaine en lignes en coupant à chaque retour à la ligne
    res = dict()
    res["x"] = ligne[0][0] # on récupère le nombre de lignes
    res["y"] = ligne[0][2] # on récupère le nombre de colonnes
    res["plateau"] = dict() # on initialise le dictionnaire qui contiendra les cases
    compt = 0 # compteur pour parcourir la chaine
    for ind_ligne in range(ligne[0][0]): # on parcourt les lignes
        for ind_colonne in range(ligne[0][2]): # on parcourt les colonnes
            if ligne[ind_ligne + 1][ind_colonne] == "#": # si on a un mur
                res["plateau"][(ind_ligne, ind_colonne)] = case.Case(True) # on ajoute une case mur
            if ligne[ind_ligne + 1][ind_colonne] == " ": # si on a une case vide
                res["plateau"][(ind_ligne, ind_colonne)] = case.Case(False) # on ajoute une case qui n'est pas un mur donc un couloir
            if ligne[ind_ligne + 1][ind_colonne] in "abcdefghijklmnopqrstuvwxyz": # si on a un mur coloré
                res["plateau"][(ind_ligne, ind_colonne)] = case.Case(True, ligne[ind_ligne + 1][ind_colonne]) # on ajoute une case mur coloré avec le joueur correspondant
            if ligne[ind_ligne + 1][ind_colonne] in "ABCDEFGHIJKLMNOPQRSTUVWXYZ": # si on a une case colorée
                res["plateau"][(ind_ligne, ind_colonne)] = case.Case(False, ligne[ind_ligne + 1][ind_colonne]) # on ajoute une case colorée avec le joueur correspondant

    return res


def Plateau(plan):
    """Créer un plateau en respectant le plan donné en paramètre.
        Le plan est une chaine de caractères contenant
            '#' (mur)
            ' ' (couloir non peint)
            une lettre majuscule (un couloir peint par le joueur représenté par la lettre)

    Args:
        plan (str): le plan sous la forme d'une chaine de caractères

    Returns:
        dict: Le plateau correspondant au plan
    """
    ligne = plan.split("\n") # on sépare la chaine en lignes en coupant à chaque retour à la ligne
    ligne2 = ligne[0].split(";") # on sépare la première ligne en deux parties en coupant à chaque point virgule
    res = dict() # on initialise le dictionnaire qui contiendra le plateau
    res["x"] = ligne2[0] # on récupère le nombre de lignes
    res["y"] = ligne2[1] # on récupère le nombre de colonnes
    res["plateau"] = dict() # on initialise le dictionnaire qui contiendra les cases
    compt = 0 # compteur pour parcourir la chaine
    for ind_ligne in range(int(ligne2[0])): # on parcourt les lignes
        for ind_colonne in range(int(ligne2[1])): # on parcourt les colonnes
            if ligne[ind_ligne + 1][ind_colonne] == "#": # si on a un mur
                res["plateau"][(ind_ligne, ind_colonne)] = case.Case(True) # on ajoute une case mur
            if ligne[ind_ligne + 1][ind_colonne] == " ": # si on a une case vide
                res["plateau"][(ind_ligne, ind_colonne)] = case.Case(False) # on ajoute une case qui n'est pas un mur donc un couloir
            if ligne[ind_ligne + 1][ind_colonne] in "abcdefghijklmnopqrstuvwxyz": # si on a un mur coloré
                res["plateau"][(ind_ligne, ind_colonne)] = case.Case(True, ligne[ind_ligne + 1][ind_colonne]) # on ajoute une case mur coloré avec le joueur correspondant
            if ligne[ind_ligne + 1][ind_colonne] in "ABCDEFGHIJKLMNOPQRSTUVWXYZ": # si on a une case colorée
                res["plateau"][(ind_ligne, ind_colonne)] = case.Case(False, ligne[ind_ligne + 1][ind_colonne]) # on ajoute une case colorée avec le joueur correspondant
    maligne = int(res["x"]) + 1 # on récupère la ligne où commence la liste des joueurs
    nb_maligne = ligne[maligne] # on récupère le nombre de joueurs
    for lignes in range(int(nb_maligne)): # on parcourt les lignes de la liste des joueurs
        malignes = ligne[int(maligne + lignes + 1)].split("\n") # on sépare la ligne en deux parties en coupant à chaque retour à la ligne

        maligne2 = malignes[0].split(';') # on sépare la ligne en deux parties en coupant à chaque point virgule
        res['plateau'][int(maligne2[1]), int(maligne2[2])]['joueurs'].add(maligne2[0]) # on ajoute le joueur à la case correspondante

    objetligne = maligne + int(ligne[maligne]) + 1 # on récupère la ligne où commence la liste des objets
    print(objetligne)
    for lignes in range(0, int(ligne[objetligne])): # on parcourt les lignes de la liste des objets
        malignes = ligne[int(objetligne) + lignes + 1].split("\n") #  on sépare la ligne en deux parties en coupant à chaque retour à la ligne

        maligne2 = malignes[0].split(';') # on sépare la ligne en deux parties en coupant à chaque point virgule
        res['plateau'][int(maligne2[1]), int(maligne2[2])]['objet'] = int(maligne2[0]) # on ajoute l'objet à la case correspondante
    return res # on retourne le plateau

fic=open('plans/plan2.txt','r')
print(Plateau(fic.read()))

def set_case(plateau, pos, une_case):
    """remplace la case qui se trouve en position pos du plateau par une_case

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire (lig,col) de deux int
        une_case (dict): la nouvelle case
    """
    plateau['plateau'][pos] = une_case # on remplace la case par une_case


def enlever_joueur(plateau, joueur, pos):
    """enlève un joueur qui se trouve en position pos sur le plateau

    Args:
        plateau (dict): le plateau considéré
        joueur (str): la lettre représentant le joueur
        pos (tuple): une paire (lig,col) de deux int

    Returns:
        bool: True si l'opération s'est bien déroulée, False sinon
    """
    if plateau['plateau'][pos]['joueurs'] != set(): # si la case contient un joueur
        plateau['plateau'][pos]['joueurs'].remove(joueur) # on enlève le joueur de la case
        return True # on retourne True
    return False    # on retourne False


def prendre_objet(plateau, pos):
    """Prend l'objet qui se trouve en position pos du plateau et retourne l'entier
        représentant cet objet. const.AUCUN indique qu'aucun objet se trouve sur case

    Args:
        plateau (dict): Le plateau considéré
        pos (tuple): une paire (lig,col) de deux int

    Returns:
        int: l'entier représentant l'objet qui se trouvait sur la case.
        const.AUCUN indique aucun objet
    """
    mon_objet = plateau['plateau'][pos]['objet'] # on récupère l'objet de la case
    if mon_objet != const.AUCUN: # si l'objet n'est pas const.AUCUN
        plateau['plateau'][pos]['objet'] = const.AUCUN # on met l'objet à const.AUCUN
        return int(mon_objet) # on retourne l'objet
    return const.AUCUN # on retourne const.AUCUN


def deplacer_joueur(plateau, joueur, pos, direction):
    """Déplace dans la direction indiquée un joueur se trouvant en position pos
        sur le plateau

    Args:
        plateau (dict): Le plateau considéré
        joueur (str): La lettre identifiant le joueur à déplacer
        pos (tuple): une paire (lig,col) d'int
        direction (str): une lettre parmie NSEO indiquant la direction du déplacement

    Returns:
        tuple: un tuple contenant 4 informations
            - un bool indiquant si le déplacement a pu se faire ou non
            - un int valeur une des 3 valeurs suivantes:
                *  1 la case d'arrivée est de la couleur du joueur
                *  0 la case d'arrivée n'est pas peinte
                * -1 la case d'arrivée est d'une couleur autre que celle du joueur
            - un int indiquant si un objet se trouvait sur la case d'arrivée (dans ce
                cas l'objet est pris de la case d'arrivée)
            - une paire (lig,col) indiquant la position d'arrivée du joueur (None si
                le joueur n'a pas pu se déplacer)
    """
    if joueur not in plateau["plateau"][pos]["joueurs"]: # si le joueur n'est pas sur la case
        couleur = plateau['plateau'][pos]['couleur'] # on récupère la couleur de la case
        if couleur != joueur: # si la couleur de la case n'est pas celle du joueur
            c = -1 # on met c à -1
        elif couleur == ' ': # si la couleur de la case est vide
            c = 0 # on met c à 0
        else: # si la couleur de la case est celle du joueur
            c = 1 # on met c à 1
        o = plateau['plateau'][pos]['objet'] # on récupère l'objet de la case
        prendre_objet(plateau, pos) # on prend l'objet de la case
        return (False, c, o, None) # on retourne False, c, o et None

    def deplacement():
        poser_joueur(plateau, joueur, nvpos) # on pose le joueur à la nouvelle position
        enlever_joueur(plateau, joueur, pos) # on enlève le joueur de la position précédente
        couleur = plateau['plateau'][nvpos]['couleur'] # on récupère la couleur de la nouvelle case
        if couleur == ' ': # si la couleur de la nouvelle case est vide
            c = 0 # on met c à 0
        elif couleur.upper() != joueur.upper(): # si la couleur de la nouvelle case n'est pas celle du joueur
            c = -1 # on met c à -1
        else: # si la couleur de la nouvelle case est celle du joueur
            c =  1 # on met c à 1
        o = plateau['plateau'][nvpos]['objet'] # on récupère l'objet de la nouvelle case
        prendre_objet(plateau, nvpos) # on prend l'objet de la nouvelle case
        return (True, c, o, nvpos) # on retourne True, c, o et nvpos

    if direction in directions_possibles(plateau, pos): # si la direction est possible
        nvpos = (pos[0] + INC_DIRECTION[direction][0], pos[1] + INC_DIRECTION[direction][1]) # on calcule la nouvelle position
        return deplacement() # on retourne le résultat de la fonction deplacement
    couleur = plateau['plateau'][pos]['couleur'] # on récupère la couleur de la case
    if couleur.upper() != joueur.upper(): # si la couleur de la case n'est pas celle du joueur
        c = -1 # on met c à -1
    elif couleur == ' ': # si la couleur de la case est vide
        c = 0 # on met c à 0
    else: # si la couleur de la case est celle du joueur
        c = 1 # on met c à 1
    o = plateau['plateau'][pos]['objet'] # on récupère l'objet de la case
    prendre_objet(plateau, pos) # on prend l'objet de la case
    return (False, c, o, None) # on retourne False, c, o et None


# -----------------------------
# fonctions d'observation du plateau
# -----------------------------

def surfaces_peintes(plateau, nb_joueurs):
    """retourne un dictionnaire indiquant le nombre de cases peintes pour chaque joueur.

    Args:
        plateau (dict): le plateau considéré
        nb_joueurs (int): le nombre de joueurs total participant à la partie

    Returns:
        dict: un dictionnaire dont les clées sont les identifiants joueurs et les
            valeurs le nombre de cases peintes par le joueur
    """
    dico = {} # on crée un dictionnaire vide
    nom_joueur = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'] # on crée une liste contenant les noms des joueurs
    for i in range(nb_joueurs): # boucle pour chaque joueur
        dico[nom_joueur[i]] = 0 # on ajoute le joueur au dictionnaire avec une valeur de 0
    for x in range(int(plateau['x'])): # boucle pour chaque ligne
        for y in range(int(plateau['y'])): # boucle pour chaque colonne
            if plateau['plateau'][x, y]['couleur'] != ' ': # si la couleur de la case n'est pas vide
                if plateau['plateau'][x, y]['couleur'].upper() not in dico.keys(): # si la couleur de la case n'est pas dans le dictionnaire
                    dico[plateau['plateau'][x, y]['couleur'].upper()] = 1 # on ajoute la couleur de la case au dictionnaire avec une valeur de 1
                else: # si la couleur de la case est dans le dictionnaire
                    dico[plateau['plateau'][x, y]['couleur'].upper()] += 1 # on ajoute 1 à la valeur de la couleur de la case
    return dico


def directions_possibles(plateau, pos):
    """ retourne les directions vers où il est possible de se déplacer à partir
        de la position pos

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): un couple d'entiers (ligne,colonne) indiquant la position de départ

    Returns:
        dict: un dictionnaire dont les clés sont les directions possibles et les valeurs la couleur
              de la case d'arrivée si on prend cette direction
              à partir de pos
    """
    dico = {}
    if (pos[0] + 1, pos[1]) in plateau['plateau'].keys() and not plateau['plateau'][pos[0] + 1, pos[1]]['mur']: # si la case du bas n'est pas un mur
        dico['S'] = plateau['plateau'][pos[0] + 1, pos[1]]['couleur'] # on ajoute la direction S au dictionnaire avec la couleur de la case du bas

    if (pos[0] - 1, pos[1]) in plateau['plateau'].keys() and not plateau['plateau'][pos[0] - 1, pos[1]]['mur']: # si la case du haut n'est pas un mur
        dico['N'] = plateau['plateau'][pos[0] - 1, pos[1]]['couleur'] # on ajoute la direction N au dictionnaire avec la couleur de la case du haut

    if (pos[0], pos[1] + 1) in plateau['plateau'].keys() and not plateau['plateau'][pos[0], pos[1] + 1]['mur']: # si la case de droite n'est pas un mur
        dico['E'] = plateau['plateau'][pos[0], pos[1] + 1]['couleur']  # on ajoute la direction E au dictionnaire avec la couleur de la case de droite

    if (pos[0], pos[1] - 1) in plateau['plateau'].keys() and not plateau['plateau'][pos[0], pos[1] - 1]['mur']: # si la case de gauche n'est pas un mur
        dico['O'] = plateau['plateau'][pos[0], pos[1] - 1]['couleur'] # on ajoute la direction O au dictionnaire avec la couleur de la case de gauche
    return dico


def nb_joueurs_direction(plateau, pos, direction, distance_max):
    """indique combien de joueurs se trouve à portée sans protection de mur.
        Attention! il faut compter les joueurs qui sont sur la case pos

    Args :
        plateau (dict): le plateau considéré
        pos (_type_): la position à partir de laquelle on fait le recherche
        direction (str): un caractère 'N','O','S','E' indiquant dans quelle direction on regarde
        distance_max (int): la distance maximale de recherche
    Returns :
        int : le nombre de joueurs à portée de peinture (ou qui risque de nous peindre)
    """
    somme = 0 # on initialise la somme à 0
    if direction=="N": # si la direction est Nord
        for i in range( distance_max + 1): # boucle pour chaque case
            if (pos[0] - i, pos[1]) in plateau['plateau'].keys() and not plateau['plateau'][pos[0] - i, pos[1]]['mur'] : # si la case est sur le plateau et n'est pas un mur
                if plateau['plateau'][pos[0] - i, pos[1]]['joueurs'] !=set(): # si la case contient un joueur
                    somme += 1 # on ajoute 1 à la somme
            else: # si la case n'est pas sur le plateau ou est un mur
                break # on arrête la boucle
    elif direction=="S": # si la direction est Sud
        for i in range( distance_max + 1): # boucle pour chaque case
            if (pos[0] + i, pos[1]) in plateau['plateau'].keys() and not plateau['plateau'][pos[0] + i, pos[1]]['mur']: # si la case est sur le plateau et n'est pas un mur
                if plateau['plateau'][pos[0] + i, pos[1]]['joueurs'] != set(): # si la case contient un joueur
                    somme += 1 # on ajoute 1 à la somme

            else:
                break
    elif direction=="E":
        for i in range( distance_max + 1):
            if (pos[0], pos[1] + i) in plateau['plateau'].keys() and not plateau['plateau'][pos[0], pos[1] + i]['mur'] :
                if plateau['plateau'][pos[0], pos[1] + i]['joueurs'] !=set():
                    somme += 1

            else:
                break
    elif direction=="O":
        for i in range( distance_max + 1):
            if (pos[0], pos[1] - i) in plateau['plateau'].keys() and not plateau['plateau'][pos[0], pos[1] - i]['mur'] :
                if plateau['plateau'][pos[0], pos[1] - i]['joueurs']!=set():
                    somme += 1
            else:
                break
    return somme


def peindre(plateau, pos, direction, couleur, reserve, distance_max, peindre_murs=False):
    """ Peint avec la couleur les cases du plateau à partir de la position pos dans
        la direction indiquée en s'arrêtant au premier mur ou au bord du plateau ou
        lorsque que la distance maximum a été atteinte.

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire (lig,col) de int
        direction (str): un des caractères 'N','S','E','O' indiquant la direction de peinture
        couleur (str): une lettre indiquant l'idenfiant du joueur qui peint (couleur de la peinture)
        reserve (int): un entier indiquant la taille de la reserve de peinture du joueur
        distance_max (int): un entier indiquant la portée maximale du pistolet à peinture
        peindre_mur (bool): un booléen indiquant si on peint aussi les murs ou non

    Returns:
        dict: un dictionnaire avec 4 clés
                "cout": un entier indiquant le cout en unités de peinture de l'action
                "nb_repeintes": un entier indiquant le nombre de cases qui ont changé de couleur
                "nb_murs_repeints": un entier indiquant le nombre de murs qui ont changé de couleur
                "joueurs_touches": un ensemble (set) indiquant les joueurs touchés lors de l'action
    """
    dico = {}
    dico['cout'] = 0 # on initialise le cout à 0
    dico['nb_repeintes'] = 0 # on initialise le nombre de cases repeintes à 0
    dico['nb_murs_repeints'] = 0 # on initialise le nombre de murs repeints à 0
    dico['joueurs_touches'] = set() # on initialise l'ensemble des joueurs touchés à vide
    if reserve < 0: # si la réserve est négative
        return dico # on retourne le dictionnaire vide
    else: # si la réserve est positive
        if reserve < distance_max: # si la réserve est inférieure à la distance maximale
            distance_max = reserve - 1 
    for ind in range(distance_max): # boucle pour chaque case
        if reserve < 0 or reserve == 0:
            break
        if (pos[0] + ind * INC_DIRECTION[direction][0], pos[1] + ind * INC_DIRECTION[direction][1]) in plateau[
            'plateau'].keys(): # si la case est sur le plateau
            if \
            plateau['plateau'][pos[0] + ind * INC_DIRECTION[direction][0], pos[1] + ind * INC_DIRECTION[direction][1]][
                'mur']: # si la case est un mur
                if not peindre_murs: # si on ne peint pas les murs
                    break
                else: # si on peint les murs
                    if plateau['plateau'][
                        pos[0] + ind * INC_DIRECTION[direction][0], pos[1] + ind * INC_DIRECTION[direction][1]][
                        'couleur'] != couleur and plateau['plateau'][
                        pos[0] + ind * INC_DIRECTION[direction][0], pos[1] + ind * INC_DIRECTION[direction][1]][
                        'couleur'] != ' ': # si la case n'est pas déjà repeinte et n'est pas blanche
                        if reserve >= 2: 
                            dico['nb_murs_repeints'] += 1
                            dico['nb_repeintes'] += 1

                            dico['cout'] += 2
                            reserve -= 2
                        plateau['plateau'][
                            pos[0] + ind * INC_DIRECTION[direction][0], pos[1] + ind * INC_DIRECTION[direction][1]][
                            'couleur'] = couleur # on repeint la case
                        dico['joueurs_touches'] = dico['joueurs_touches'] | plateau['plateau'][
                            pos[0] + ind * INC_DIRECTION[direction][0], pos[1] + ind * INC_DIRECTION[direction][1]][
                            'joueurs'] # on ajoute les joueurs présents sur la case à l'ensemble des joueurs touchés
                    elif plateau['plateau'][
                        pos[0] + ind * INC_DIRECTION[direction][0], pos[1] + ind * INC_DIRECTION[direction][1]][
                        'couleur'] == ' ': # si la case est blanche
                        if reserve >= 1: 
                            dico['nb_murs_repeints'] += 1
                            dico['nb_repeintes'] += 1

                            dico['cout'] += 1
                            reserve -= 1
                        plateau['plateau'][
                            pos[0] + ind * INC_DIRECTION[direction][0], pos[1] + ind * INC_DIRECTION[direction][1]][
                            'couleur'] = couleur # on repeint la case
                        dico['joueurs_touches'] = dico['joueurs_touches'] | plateau['plateau'][
                            pos[0] + ind * INC_DIRECTION[direction][0], pos[1] + ind * INC_DIRECTION[direction][1]][
                            'joueurs'] # on ajoute les joueurs présents sur la case à l'ensemble des joueurs touchés
                    elif plateau['plateau'][
                        pos[0] + ind * INC_DIRECTION[direction][0], pos[1] + ind * INC_DIRECTION[direction][1]][
                        'couleur'] == couleur: # si la case est déjà repeinte
                        if reserve >= 1: 
                            dico['nb_murs_repeints'] += 0

                            dico['cout'] += 1
                            reserve -= 1
                        plateau['plateau'][
                            pos[0] + ind * INC_DIRECTION[direction][0], pos[1] + ind * INC_DIRECTION[direction][1]][
                            'couleur'] = couleur # on repeint la case
                        dico['joueurs_touches'] = dico['joueurs_touches'] | plateau['plateau'][
                            pos[0] + ind * INC_DIRECTION[direction][0], pos[1] + ind * INC_DIRECTION[direction][1]][
                            'joueurs'] # on ajoute les joueurs présents sur la case à l'ensemble des joueurs touchés
            else:
                if plateau['plateau'][
                    pos[0] + ind * INC_DIRECTION[direction][0], pos[1] + ind * INC_DIRECTION[direction][1]][
                    'couleur'] != couleur and plateau['plateau'][
                    pos[0] + ind * INC_DIRECTION[direction][0], pos[1] + ind * INC_DIRECTION[direction][1]][
                    'couleur'] != ' ': # si la case n'est pas déjà repeinte et n'est pas blanche
                    if reserve >= 2:
                        dico['nb_repeintes'] += 1

                        dico['cout'] += 2
                        reserve -= 2
                    plateau['plateau'][
                        pos[0] + ind * INC_DIRECTION[direction][0], pos[1] + ind * INC_DIRECTION[direction][1]][
                        'couleur'] = couleur 
                    dico['joueurs_touches'] = dico['joueurs_touches'] | plateau['plateau'][
                        pos[0] + ind * INC_DIRECTION[direction][0], pos[1] + ind * INC_DIRECTION[direction][1]][
                        'joueurs']
                elif plateau['plateau'][
                    pos[0] + ind * INC_DIRECTION[direction][0], pos[1] + ind * INC_DIRECTION[direction][1]][
                    'couleur'] == ' ':
                    if reserve >= 1:
                        dico['nb_repeintes'] += 1

                        dico['cout'] += 1
                        reserve -= 1
                    plateau['plateau'][
                        pos[0] + ind * INC_DIRECTION[direction][0], pos[1] + ind * INC_DIRECTION[direction][1]][
                        'couleur'] = couleur
                    dico['joueurs_touches'] = dico['joueurs_touches'] | plateau['plateau'][
                        pos[0] + ind * INC_DIRECTION[direction][0], pos[1] + ind * INC_DIRECTION[direction][1]][
                        'joueurs']
                elif plateau['plateau'][
                    pos[0] + ind * INC_DIRECTION[direction][0], pos[1] + ind * INC_DIRECTION[direction][1]][
                    'couleur'] == couleur:
                    if reserve >= 1:
                        dico['nb_repeintes'] += 0

                        dico['cout'] += 1
                        reserve -= 1
                    plateau['plateau'][
                        pos[0] + ind * INC_DIRECTION[direction][0], pos[1] + ind * INC_DIRECTION[direction][1]][
                        'couleur'] = couleur
                    dico['joueurs_touches'] = dico['joueurs_touches'] | plateau['plateau'][
                        pos[0] + ind * INC_DIRECTION[direction][0], pos[1] + ind * INC_DIRECTION[direction][1]][
                        'joueurs']

    return dico # on retourne le dictionnaire